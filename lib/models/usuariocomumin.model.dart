class UsuarioComumIn {
  String username;
  String nomecompleto;
  String telefone;
  String documentoidentificacao;
  String senha;

  UsuarioComumIn(this.username, this.nomecompleto, this.telefone,
      this.documentoidentificacao, this.senha);

  @override
  String toString() {
    return 'UsuarioComumIn{username: $username, nomecompleto: $nomecompleto, telefone: $telefone, documentoidentificacao: $documentoidentificacao, senha: $senha}';
  }
}