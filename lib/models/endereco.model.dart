class Endereco {
  final String Logradouro;
  final String Bairro;
  final String Cep;
  final String Numero;
  final String Complemento;
  final String Cidade;
  final String Estado;

  Endereco(
      {this.Logradouro,
      this.Bairro,
      this.Cep,
      this.Numero,
      this.Complemento,
      this.Cidade,
      this.Estado});

  @override
  String toString() {
    return 'Endereco{Logradouro: $Logradouro, Bairro: $Bairro, Cep: $Cep, Numero: $Numero, Complemento: $Complemento, Cidade: $Cidade, Estado: $Estado}';
  }
}
