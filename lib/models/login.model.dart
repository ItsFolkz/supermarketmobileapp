import 'package:supermarketdeliveryapp/models/enums/tipologin.enum.dart';

class Login {
  final String username;
  final String senha;
  final TipoLoginEnum tipo;

  Login(this.username, this.senha, this.tipo);
}