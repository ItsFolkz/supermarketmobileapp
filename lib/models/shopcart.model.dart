import 'package:supermarketdeliveryapp/models/shopitem.model.dart';

class ShopCart {
  int Id;
  List<ShopItem> Items;
  double ValorTotal;

  ShopCart({this.Id, this.Items, this.ValorTotal});
}