import 'package:supermarketdeliveryapp/models/enums/tipologin.enum.dart';
import 'package:supermarketdeliveryapp/models/usuariocomumin.model.dart';

class RegisterIn {
  UsuarioComumIn comum;



  TipoLoginEnum tipo;

  RegisterIn({this.comum, this.tipo});

  @override
  String toString() {
    String usrComum = comum.toString();
    return 'RegisterIn{comum: $usrComum., tipo: $tipo}';
  }
}