

import 'package:supermarketdeliveryapp/models/produto.model.dart';

class ShopItem {
  int Id;
  Produto produto;
  int Quantidade;
  double ValorTotal;

  ShopItem({this.Id, this.produto, this.Quantidade, this.ValorTotal});
}