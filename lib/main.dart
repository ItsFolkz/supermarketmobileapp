import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:supermarketdeliveryapp/models/shopcart.model.dart';
import 'package:supermarketdeliveryapp/models/shopitem.model.dart';
import 'package:supermarketdeliveryapp/pages/login.page.dart';
import 'package:supermarketdeliveryapp/pages/sectionproducts.page.dart';

import 'models/endereco.model.dart';
import 'models/produto.model.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]).then((_) => runApp(SuperMarketDelviery()));
}

class SuperMarketDelviery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          backgroundColor: Colors.white,
          primaryColor: Colors.blue[900],
          accentColor: Colors.redAccent[700],
          buttonTheme: ButtonThemeData(
              buttonColor: Colors.red[500],
              textTheme: ButtonTextTheme.primary)),
      home: LoginPage(),
    );
  }
}
