import 'package:flutter/material.dart';

class EnderecosPage extends StatefulWidget {
  final String label;

  EnderecosPage({this.label});

  @override
  _EnderecosPageState createState() => _EnderecosPageState();
}

class _EnderecosPageState extends State<EnderecosPage> {
  bool _showNewAddress;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _showNewAddress = false;
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.label),
      ),
      body: Stack(
        children: <Widget>[
          PageHeader(
            showNewAddress: _showNewAddress,
            onTap: () {
              setState(() {
                _showNewAddress = !_showNewAddress;
              });
            },
          ),
          PageForm(
            top: screenHeight * 0.07,
            showNewAddress: _showNewAddress,
          ),
          PageContent(
            top: !_showNewAddress ? screenHeight * 0.07 : screenHeight * 0.7,
          )
        ],
      ),
    );
  }
}

class PageForm extends StatelessWidget {
  final double top;
  final bool showNewAddress;

  PageForm({this.top, this.showNewAddress});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: top,
      right: 0,
      left: 0,
      child: AnimatedOpacity(
        duration: Duration(milliseconds: 200),
        opacity: showNewAddress ? 1 : 0,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.6,
            child: Column(
              children: <Widget>[
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Estado'),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Cidade'),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Bairro'),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.47,
                      child: TextField(
                        decoration: InputDecoration(labelText: 'Logradouro'),
                      ),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.25,
                      child: TextField(
                        decoration: InputDecoration(labelText: 'Numero'),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Complemento'),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.34,
                      child: TextField(
                        decoration: InputDecoration(labelText: 'Telefone'),
                      ),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.4,
                      child: TextField(
                        decoration: InputDecoration(labelText: 'Responsável'),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: FlatButton(
                      child: Text(
                        'Salvar',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: Colors.blue),
                      ),
                      onPressed: () { },
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PageContent extends StatelessWidget {
  final GestureDragUpdateCallback onPanUpdate;
  final double top;

  PageContent({this.onPanUpdate, this.top});

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return AnimatedPositioned(
      duration: Duration(milliseconds: 300),
      curve: Curves.easeOut,
      top: top,
      height: screenHeight * 0.8,
      width: MediaQuery.of(context).size.width,
      child: Container(
        padding: EdgeInsets.all(8.0),
        child: ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(
                Icons.contact_mail,
                size: 40,
              ),
              title: Text('Casa'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                      'Avenida Teste 1 nº 458, Bloco 4 Ap 102',
                      style: TextStyle(fontSize: 12)),
                  Text('Testando - Itajubá', style: TextStyle(fontSize: 12)),
                  Text('(35) 9-984552536', style: TextStyle(fontSize: 12))
                ],
              ),
            ),
            Divider(),
            ListTile(
              leading: Icon(
                Icons.contact_mail,
                size: 40,
              ),
              title: Text('Trabalho'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Avenida Teste 2 nº 55, ', style: TextStyle(fontSize: 12)),
                  Text('Testando - Itajubá', style: TextStyle(fontSize: 12)),
                  Text('(35) 9-999998888', style: TextStyle(fontSize: 12))
                ],
              ),
            ),
            Divider(),
            ListTile(
              leading: Icon(
                Icons.contact_mail,
                size: 40,
              ),
              title: Text('Namorada'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Avenida Teste nº 55, Bloco 3 Ap 202',
                      style: TextStyle(fontSize: 12)),
                  Text('Testando - Itajubá', style: TextStyle(fontSize: 12)),
                  Text('(35) 9-955554444', style: TextStyle(fontSize: 12))
                ],
              ),
            ),
            Divider(),
          ],
        ),
      ),
    );
  }
}

class PageHeader extends StatelessWidget {
  final bool showNewAddress;
  final VoidCallback onTap;

  PageHeader({this.showNewAddress, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        GestureDetector(
          onTap: onTap,
          child: Container(
            padding: EdgeInsets.all(8),
            child: Align(
              alignment: Alignment.topCenter,
              child: Column(
                children: <Widget>[
                  Text(
                    'Novo Endereço',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.blue),
                  ),
                  Icon(
                    !showNewAddress ? Icons.expand_more : Icons.expand_less,
                    color: Colors.blue,
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
