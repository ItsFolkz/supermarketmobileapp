import 'package:flutter/material.dart';

import 'login.page.dart';

class RecuperarSenhaPage extends StatefulWidget {
  @override
  _RecuperarSenhaPageState createState() => _RecuperarSenhaPageState();
}

class _RecuperarSenhaPageState extends State<RecuperarSenhaPage> {
  String _username;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Recuperar Senha'),
      ),
      body: Container(
        padding: EdgeInsets.only(
          top: 10,
          left: 40,
          right: 40,
        ),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Logo(),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'JaneDoe79',
                  labelText: 'Usuário',
                ),
                validator: (String value) => _validarUsername(value),
                onSaved: (String value) {
                  _username = value;
                },
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                child: Text('Digite seu Usuário e clique em recuperar.'),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                height: 60,
                padding: EdgeInsets.only(
                  left: 60,
                  right: 60,
                ),
                child: RaisedButton(
                  onPressed: () => _recuperarSenha(context),
                  child: Text(
                    'Recuperar',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 20,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _recuperarSenha(BuildContext context) {
    if (!_formKey.currentState.validate()) return;
    _formKey.currentState.save();
    debugPrint('Recuperar Senha - Username: $_username');
//    if (_username != null) {
//
//    }
    //    Navigator.pop(context, register);
  }

  String _validarUsername(String value) {
    if (value.length >= 10) {
      debugPrint('ValidaUsername (10)');
      return 'Your username is too big, try something smaller';
    } else if (value.length <= 3) {
      debugPrint('ValidaUsername (3)');
      return 'Your username is too small, try something bigger';
    }
    debugPrint('Value = $value');
  }

}
