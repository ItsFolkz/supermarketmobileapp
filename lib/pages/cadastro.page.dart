import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:supermarketdeliveryapp/models/registerin.model.dart';
import 'package:supermarketdeliveryapp/models/usuariocomumin.model.dart';

import 'login.page.dart';

class CadastroPage extends StatefulWidget {
  @override
  _CadastroPageState createState() => _CadastroPageState();
}

class _CadastroPageState extends State<CadastroPage> {
  String _username;
  String _password;
  String _nomeCompleto;
  String _documentoIdentificacao;
  String _telefone;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cadastro'),
      ),
      body: Container(
        padding: EdgeInsets.only(
          top: 10,
          left: 40,
          right: 40,
        ),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Logo(),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'JaneDoe79',
                  labelText: 'Usuário',
                ),
                validator: (String value) => _validarUsername(value),
                onSaved: (String value) {
                  _username = value;
                },
              ),
              TextFormField(
                obscureText: true,
                decoration: InputDecoration(labelText: 'Senha'),
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'JaneDoe@test.com',
                  labelText: 'E-mail',
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Jane Doe',
                  labelText: 'Nome Completo',
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: '000.000.000-00',
                  labelText: 'Documento de Identificação',
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: '(00) 90000-0000',
                  labelText: 'Telefone',
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                height: 60,
                padding: EdgeInsets.only(
                  left: 60,
                  right: 60,
                ),
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                child: SizedBox.expand(
                  child: RaisedButton(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Cadastrar',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 20,
                          ),
                          textAlign: TextAlign.left,
                        ),
                        Container(
                          child: SizedBox(
                            child: Icon(Icons.archive),
                          ),
                        ),
                      ],
                    ),
                    onPressed: () => _cadastrar(context),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _cadastrar(BuildContext context) {
    RegisterIn register = RegisterIn();
    if (!_formKey.currentState.validate()) return;
    _formKey.currentState.save();
    if (_username != null &&
        _nomeCompleto != null &&
        _password != null &&
        _telefone != null &&
        _documentoIdentificacao != null) {
      register.comum = UsuarioComumIn(_username, _nomeCompleto, _telefone,
          _documentoIdentificacao, _password);
    }
//    Navigator.pop(context, register);
    String teste = register.comum.toString();
    debugPrint('Register.Comum: $teste');
  }

  String _validarUsername(String value) {
    if (value.length >= 10) {
//      debugPrint('ValidaUsername (10)');
      return 'Your username is too big, try something smaller';
    }
    if (value.length <= 3) {
//      debugPrint('ValidaUsername (3)');
      return 'Your username is too small, try something bigger';
    }
  }
}
