import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:supermarketdeliveryapp/main.dart';
import 'package:supermarketdeliveryapp/pages/home.page.dart';
import 'package:supermarketdeliveryapp/pages/recuperarsenha.page.dart';

import 'cadastro.page.dart';

String username;
String password;
final String _username = 'Admin';
final String _password = '123456';
final formKey = GlobalKey<FormState>();
final TextEditingController _usernameController = TextEditingController();
final TextEditingController _passwordController = TextEditingController();

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FormLogin(),
    );
  }
}

_validarUsername(String value) {
//  if (value.length > 10) {
////      debugPrint('ValidaUsername (10)');
//    return 'Your username is too big, try something smaller';
//  }
  if (value.length <= 3) {
    return 'Seu usuário deve ter mais de 3 caracteres!';
  }
}

_validarPassword(String value) {
  if (value.length <= 3) {
    return 'Sua senha está incorreta, tenta novamente!';
  }
}

class FormLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: 60,
        left: 40,
        right: 40,
      ),
      child: Form(
        key: formKey,
        child: ListView(
          children: <Widget>[
            Logo(),
            SizedBox(
              height: 15,
            ),
            TextFormField(
              controller: _usernameController,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                labelText: 'Usuário',
                labelStyle: TextStyle(
                  color: Colors.black38,
                  fontWeight: FontWeight.w400,
                  fontSize: 20,
                ),
              ),
              validator: (String value) => _validarUsername(value),
              onSaved: (String value) {
                username = value;
              },
              style: TextStyle(fontSize: 20),
            ),
            TextFormField(
              controller: _passwordController,
              keyboardType: TextInputType.text,
              obscureText: true,
              decoration: InputDecoration(
                labelText: 'Senha',
                labelStyle: TextStyle(
                  color: Colors.black38,
                  fontWeight: FontWeight.w400,
                  fontSize: 20,
                ),
              ),
//              validator: (String value) => _validarPassword(value),
              onSaved: (String value) {
                password = value;
              },
              style: TextStyle(fontSize: 20),
            ),
            LoginFlatButtons(
              label: 'Recuperar Senha',
              alinhamentoContainer: Alignment.centerRight,
            ),
            SizedBox(
              height: 40,
            ),
            LoginButton(
              label: 'Entrar',
            ),
            SizedBox(
              height: 10,
            ),
            LoginFlatButtons(
              label: 'Cadastre-se',
              isRegister: 'true',
            ),
          ],
        ),
      ),
    );
  }
}

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 170,
      height: 170,
      child: Image.asset("assets/logo.png"),
    );
  }
}

class LoginTextFields extends StatelessWidget {
  final TextEditingController controller;
  final String label;
  final TextInputType keyboardType;
  final String isPassword;

  LoginTextFields(
      {this.controller, this.label, this.keyboardType, this.isPassword});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      keyboardType: keyboardType,
      obscureText: isPassword == 'true' ? true : false,
      decoration: InputDecoration(
        labelText: label,
        labelStyle: TextStyle(
          color: Colors.black38,
          fontWeight: FontWeight.w400,
          fontSize: 20,
        ),
      ),
      style: TextStyle(fontSize: 20),
    );
  }
}

class LoginFlatButtons extends StatelessWidget {
  final String label;
  final Alignment alinhamentoContainer;
  final String isRegister;

  LoginFlatButtons({this.label, this.alinhamentoContainer, this.isRegister});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      alignment: alinhamentoContainer != null ? alinhamentoContainer : null,
      child: FlatButton(
        child: Text(
          label,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 16,
          ),
        ),
        onPressed: () => {
          if (isRegister == 'true')
            {_registerPage(context)}
          else
            {_retrievePassword(context)}
        },
      ),
    );
  }

  void _registerPage(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return CadastroPage();
    }));
    debugPrint('Pagina de Cadastro');
//      Navigator.pop(context, transferenciaCriada);
  }

  void _retrievePassword(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return RecuperarSenhaPage();
    }));
    debugPrint('Pagina para Recuperar senha');
  }
}

class LoginButton extends StatelessWidget {
  final String label;

  LoginButton({this.label});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: EdgeInsets.only(
        left: 60,
        right: 60,
      ),
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: SizedBox.expand(
        child: RaisedButton(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                label,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 20,
                ),
                textAlign: TextAlign.left,
              ),
              Container(
                child: SizedBox(
                  child: Image.asset("assets/sign-in.png"),
                  height: 28,
                  width: 28,
                ),
              ),
            ],
          ),
          onPressed: () => _validarLogin(context),
        ),
      ),
    );
  }

  _validarLogin(BuildContext context) {
    if (!formKey.currentState.validate()) return;
    formKey.currentState.save();
    debugPrint('UserName: $username, Password: $password');
    if (username == _username && password == _password) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    } else {
      final alertDialog = AlertDialog(
        title: Text(
          'Login mal sucedido',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          textAlign: TextAlign.center,
        ),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(
                'Usuário e/ou Senha estão incorretos.',
                textAlign: TextAlign.left,
              )
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok'),
            onPressed: () => {Navigator.of(context).pop()},
          ),
        ],
      );

      return showDialog(
          context: context,
          builder: (context) {
            return alertDialog;
          });
    }
  }
}
