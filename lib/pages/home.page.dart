import 'package:flutter/material.dart';
import 'package:supermarketdeliveryapp/models/produto.model.dart';
import 'package:supermarketdeliveryapp/pages/login.page.dart';
import 'package:supermarketdeliveryapp/pages/sectionproducts.page.dart';
import 'package:supermarketdeliveryapp/pages/shoppingcart.page.dart';

import '../main.dart';
import 'endereco.page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Página Inicial'),
      ),
      drawer: SizedBox(
        width: 200,
        child: Drawer(
          child: ListView(
            children: <Widget>[
              UserAccountsDrawerHeader(
                accountName: Text('JaneDoe98'),
                accountEmail: Text('JaneDoe@live.com'),
                currentAccountPicture: CircleAvatar(
                  child: SizedBox(
                    height: 50,
                    width: 50,
                    child: Image.asset('assets/profile-pic.png'),
                  ),
                ),
              ),
//              ListTile(
//                leading: Icon(Icons.home),
//                title: Text('Home'),
//              ),
              ListTile(
                leading: Icon(Icons.shopping_cart),
                title: Text('Carrinho'),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Carrinho()));
                },
              ),
              ListTile(
                leading: Icon(Icons.contact_mail),
                title: Text('Endereço'),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => EnderecosPage(
                        label: 'Endereço',
                      ),
                    ),
                  );
                },
              ),
//              ListTile(
//                leading: Icon(Icons.credit_card),
//                title: Text('Cartões'),
//              ),
              ListTile(
                leading: Icon(Icons.arrow_forward),
                title: Text('Sair'),
                onTap: () {
//                  Navigator.of(context).pop();
                  Navigator.popUntil(context, ModalRoute.withName("/"));
//                  Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
                },
              ),
            ],
          ),
        ),
      ),
      body: PageContent(),
    );
  }
}

class PageContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final _screenHeight = MediaQuery.of(context).size.height;
    return Stack(
      children: <Widget>[
        Container(
          child: Column(
            children: <Widget>[
              SizedBox(height: 30),
              PromoCards(),
              SizedBox(
                height: 10,
              ),
              Logo(),
            ],
          ),
        ),
        MenuBarBottom()
      ],
    );
  }
}

class PromoCards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      child: PageView(
        controller: PageController(viewportFraction: 0.8),
        scrollDirection: Axis.horizontal,
        pageSnapping: true,
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 8),
            decoration: BoxDecoration(
              border: Border.all(),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(border: Border.all()),
                    height: 120,
                    width: 120,
                    child: Center(
                        child: Text(
                      'Imagem Produto',
                      textAlign: TextAlign.center,
                    )),
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(
                            top: 10, left: 50, right: 5, bottom: 5),
                        child: Text(
                          'Marca Produto',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                      Container(
                        padding:
                            const EdgeInsets.only(top: 10, left: 5, bottom: 5),
                        child: Text(
                          'Nome Produto',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(
                            top: 5, left: 70, right: 15, bottom: 10),
                        child: Text(
                          'Desconto',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 5, left: 15, bottom: 10),
                        child: Text(
                          'Data Término',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(
                            top: 5, left: 70, right: 15, bottom: 10),
                        child: Text(
                          'De 120,00',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.lineThrough),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 5, left: 15, bottom: 10),
                        child: Text(
                          'Por 85,99',
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.red[900],
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 5,
                      left: 40,
                      right: 40,
                    ),
                    child: Container(
                      height: 50,
                      alignment: Alignment.center,
                      child: FlatButton(
                        child: Text(
                          'Adcionar ao Carrinho',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        onPressed: () => {},
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 8),
            decoration: BoxDecoration(
              border: Border.all(),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(border: Border.all()),
                    height: 120,
                    width: 120,
                    child: Center(
                        child: Text(
                      'Imagem Produto',
                      textAlign: TextAlign.center,
                    )),
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(
                            top: 10, left: 50, right: 5, bottom: 5),
                        child: Text(
                          'Marca Produto',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                      Container(
                        padding:
                            const EdgeInsets.only(top: 10, left: 5, bottom: 5),
                        child: Text(
                          'Nome Produto',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(
                            top: 5, left: 70, right: 15, bottom: 10),
                        child: Text(
                          'Desconto',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 5, left: 15, bottom: 10),
                        child: Text(
                          'Data Término',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(
                            top: 5, left: 70, right: 15, bottom: 10),
                        child: Text(
                          'De 120,00',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.lineThrough),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 5, left: 15, bottom: 10),
                        child: Text(
                          'Por 85,99',
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.red[900],
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 5,
                      left: 40,
                      right: 40,
                    ),
                    child: Container(
                      height: 50,
                      alignment: Alignment.center,
                      child: FlatButton(
                        child: Text(
                          'Adcionar ao Carrinho',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        onPressed: () => {},
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 8),
            decoration: BoxDecoration(
              border: Border.all(),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(border: Border.all()),
                    height: 120,
                    width: 120,
                    child: Center(
                        child: Text(
                      'Imagem Produto',
                      textAlign: TextAlign.center,
                    )),
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(
                            top: 10, left: 50, right: 5, bottom: 5),
                        child: Text(
                          'Marca Produto',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                      Container(
                        padding:
                            const EdgeInsets.only(top: 10, left: 5, bottom: 5),
                        child: Text(
                          'Nome Produto',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(
                            top: 5, left: 70, right: 15, bottom: 10),
                        child: Text(
                          'Desconto',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 5, left: 15, bottom: 10),
                        child: Text(
                          'Data Término',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(
                            top: 5, left: 70, right: 15, bottom: 10),
                        child: Text(
                          'De 120,00',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.lineThrough),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 5, left: 15, bottom: 10),
                        child: Text(
                          'Por 85,99',
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.red[900],
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 5,
                      left: 40,
                      right: 40,
                    ),
                    child: Container(
                      height: 50,
                      alignment: Alignment.center,
                      child: FlatButton(
                        child: Text(
                          'Adcionar ao Carrinho',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        onPressed: () => {},
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MenuBarBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0 + MediaQuery.of(context).padding.bottom,
      left: 0,
      right: 0,
      height: MediaQuery.of(context).size.height * 0.165,
      child: Container(
        child: ListView(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            MenuItemBottom(
              label: 'Açougue',
              image: Image.asset(
                'assets/section-acougue.png',
                height: 30,
                width: 30,
              ),
            ),
            MenuItemBottom(
              label: 'Hortifruti',
              image: Image.asset(
                'assets/section-hortifruti.png',
                height: 30,
                width: 30,
              ),
            ),
            MenuItemBottom(
              label: 'Bebidas',
              image: Image.asset(
                'assets/section-bebidas.png',
                height: 30,
                width: 30,
              ),
            ),
            MenuItemBottom(
              label: 'Frios',
              image: Image.asset(
                'assets/section-frios.png',
                height: 30,
                width: 30,
              ),
            ),
            MenuItemBottom(
              label: 'Limpeza',
              image: Image.asset(
                'assets/section-limpeza.png',
                height: 30,
                width: 30,
              ),
            ),
            MenuItemBottom(
              label: 'Higiene',
              image: Image.asset(
                'assets/section-higienepessoal.png',
                height: 30,
                width: 30,
              ),
            ),
            MenuItemBottom(
              label: 'Padaria',
              image: Image.asset(
                'assets/section-padaria.png',
                height: 30,
                width: 30,
              ),
            ),
            MenuItemBottom(
              label: 'Mercearia',
              image: Image.asset(
                'assets/section-mercearia.png',
                height: 30,
                width: 30,
              ),
            ),
            MenuItemBottom(
              label: 'Doces',
              image: Image.asset(
                'assets/section-doces.png',
                height: 30,
                width: 30,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MenuItemBottom extends StatelessWidget {
  final String label;
  final Image image;

  MenuItemBottom({this.label, this.image});

  @override
  Widget build(BuildContext context) {
    List<Produto> sectionProducts = List<Produto>();
    if (label == 'Açougue') {
      for (int i = 0; i < 15; i++) {
        String number = (8.99 + i).toStringAsFixed(2);
        Produto item =
            Produto(Nome: 'Carne ${i + 1}', Valor: double.tryParse(number), Marca: 'Sadia ${i + 1}');
        sectionProducts.add(item);
      }
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 12),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.26,
        child: FlatButton(
          padding: EdgeInsets.all(6),
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: image,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      label,
                      style: TextStyle(
                          color: Colors.blue[900],
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.blue[100]),
          ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SectionProducts(
                    sectionName: label, sectionProducts: sectionProducts),
              ),
            );
          },
        ),
      ),
    );
  }
}
