import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:supermarketdeliveryapp/models/produto.model.dart';

class SectionProducts extends StatefulWidget {
  final String sectionName;
  List<Produto> sectionProducts = List<Produto>();

  SectionProducts({this.sectionName, this.sectionProducts});

  @override
  _SectionProductsState createState() => _SectionProductsState();
}

class _SectionProductsState extends State<SectionProducts> {
  String filterText = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(widget.sectionName),
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    icon: Icon(
                      Icons.search,
                      color: Colors.blue[900],
                    ),
                    hintText: 'Procure seu produto...',
                    labelText: 'Nome do Produto',
                  ),
                  onChanged: (text) {
                    setState(() {
                      filterText = text;
                    });
                  },
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                height: MediaQuery.of(context).size.height * 0.79,
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: SectionItemList(
                    sectionName: widget.sectionName,
                    sectionProducts: widget.sectionProducts,
                    filterText: filterText,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ProductInfo extends StatelessWidget {
  final Produto produto;

  ProductInfo({this.produto});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detalhes do Produto'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              SizedBox(height: 5),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.8,
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Nome',
                    labelStyle: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  enabled: false,
                  initialValue: '${produto.Nome}',
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.8,
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Marca',
                    labelStyle: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  enabled: false,
                  initialValue: '${produto.Marca}',
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.8,
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Valor Unitário',
                    labelStyle: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  enabled: false,
                  initialValue: r'R$ ' + '${produto.Valor}',
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.5,
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Descrição',
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 18),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus porttitor purus non iaculis. Vestibulum mattis tempor faucibus. Pellentesque hendrerit consectetur fringilla. Praesent placerat sed tellus id eleifend. Vestibulum feugiat lacinia arcu in ultricies. In tincidunt euismod feugiat. In aliquet ultricies lorem vitae ultrices. In tristique augue erat, sed condimentum sem condimentum vel. Fusce sed tortor eros. Mauris lacus dui, mattis quis nunc ac, faucibus facilisis ante.',
                          style: TextStyle(fontSize: 14),
                          textAlign: TextAlign.justify,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SectionItemList extends StatefulWidget {
  final String sectionName;
  final List<Produto> sectionProducts;
  final String filterText;

  SectionItemList({this.sectionName, this.sectionProducts, this.filterText});

  @override
  _SectionItemListState createState() => _SectionItemListState();
}

class _SectionItemListState extends State<SectionItemList> {
  final bool showDetail;

  _SectionItemListState({this.showDetail});

  @override
  Widget build(BuildContext context) {
    if (widget.sectionProducts.length == 0) {
      return ListView(
        children: <Widget>[
          ListTile(
            title: Text('Nenhum produto nessa seção'),
          )
        ],
      );
    }
    List<Produto> listaFiltrada = List<Produto>();
    if (widget.filterText.isNotEmpty) {
      for (Produto item in widget.sectionProducts) {
        String nome = item.Nome.toLowerCase();
        if (nome.contains(widget.filterText.toLowerCase())) {
          listaFiltrada.add(item);
        }
      }
    } else {
      listaFiltrada.addAll(widget.sectionProducts);
    }

    return ListView.builder(
      itemCount: listaFiltrada.length,
      itemBuilder: (BuildContext context, int index) {
        Produto item = listaFiltrada[index];
        return Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.2,
          closeOnScroll: true,
          child: ListTile(
            leading: GestureDetector(
              child: Image.asset(
                'assets/section-acougue.png',
                height: 30,
                width: 30,
              ),
              // Icon(Icons.add, color: Colors.blue[100], size: 40),
              onTap: () {
                debugPrint('Abriu');
              },
            ),
            title: Text(item.Nome),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Marca: ${item.Marca}',
                  style: TextStyle(
                      color: Colors.red[900], fontWeight: FontWeight.w500),
                ),
                Row(
                  children: <Widget>[
                    Text(
                      r'Valor Un. R$',
                      style: TextStyle(
                          color: Colors.black54, fontWeight: FontWeight.w500),
                    ),
                    Text('${item.Valor}',
                      style: TextStyle(
                          color: Colors.black54, fontWeight: FontWeight.w500),)
                  ],
                ),
              ],
            ),
            trailing: Icon(Icons.arrow_forward_ios),
          ),
          secondaryActions: <Widget>[
            IconSlideAction(
              caption: 'Comprar',
              icon: Icons.add,
              color: Colors.green[300],
              onTap: () {
                debugPrint('Add');
              },
            ),
            IconSlideAction(
              caption: 'Detalhes',
              icon: Icons.info_outline,
              color: Colors.red[200],
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProductInfo(
                        produto: item,
                      ),
                    ));
              },
            ),
          ],
        );
      },
    );
  }
}
