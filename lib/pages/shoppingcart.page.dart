import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:supermarketdeliveryapp/models/produto.model.dart';
import 'package:supermarketdeliveryapp/models/shopcart.model.dart';
import 'package:supermarketdeliveryapp/models/shopitem.model.dart';

class Carrinho extends StatefulWidget {
  @override
  _CarrinhoState createState() => _CarrinhoState();
}

class _CarrinhoState extends State<Carrinho> {
  bool _showMyAddress;
  String _isRadioSelected;
  String groupValue;
  String value;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _showMyAddress = true;
    _isRadioSelected = 'Cartão de Crédito';
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    double valorTotal = 0;
    ShopCart cart;
    List<ShopItem> cartItems = List<ShopItem>();
    for (int i = 0; i < 8; i++) {
      String number = (8.99 + i).toStringAsFixed(2);
      Produto prod = Produto(
          Nome: 'Carne ${i + 1}',
          Valor: double.tryParse(number),
          Marca: 'Sadia ${i + 1}');
      ShopItem item = ShopItem(
          produto: prod,
          Quantidade: (i + 1),
          ValorTotal: (prod.Valor * (i + 1)));
      valorTotal += item.ValorTotal;
      cartItems.add(item);
    }
    cart = ShopCart(Items: cartItems, ValorTotal: valorTotal);
    return Scaffold(
      appBar: AppBar(
        title: Text('Carrinho'),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: screenHeight * 0.1,
                width: screenWidth,
                color: Colors.blue[100],
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            r'Valor Total: R$',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 22,
                                color: Colors.blue[900]),
                          ),
                          Text(
                            '${cart.ValorTotal.toStringAsFixed(2)}',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 22,
                                color: Colors.blue[900]),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            ShopCartList(
              shopCart: cart,
              total: valorTotal,
            ),
            SizedBox(
              height: 10,
            ),
//            Center(
//              child: Text(
//                'Dados do Cartão',
//                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
//              ),
//            ),
            Padding(
              padding:
                  const EdgeInsets.only(left: 0, right: 0, top: 5, bottom: 5),
              child: Center(
                child: Text(
                  'Endereço para Entrega',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
                ),
              ),
            ),
            MyAddressCards(
              top: screenHeight * 0.5,
            ),
//            PageContent(
//              top: screenHeight * 0.5,
//            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 50,
                padding: EdgeInsets.only(left: screenWidth * 0.111),
                child: Row(
                  children: <Widget>[
                    Radio(
                      groupValue: _isRadioSelected,
                      value: 'Cartão de Crédito',
                      onChanged: (newValue) {
                        setState(() {
                          _isRadioSelected = newValue;
                        });
                      },
                    ),
                    Text('Cartão de Crédito', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),),
                    Radio(
                      groupValue: _isRadioSelected,
                      value: 'Dinheiro',
                      onChanged: (newValue) {
                        setState(() {
                          _isRadioSelected = newValue;
                        });
                      },
                    ),
                    Text('Dinheiro', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(left: 8, right: 8, bottom: 10, top: 0),
                child: Container(
                  height: screenHeight * 0.08,
                  child: FlatButton(
                    child: Text(
                      'Finalizar Compra',
                      style: TextStyle(fontSize: 18),
                    ),
                    onPressed: () {},
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CreditCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 0, left: 8, bottom: 0, right: 8),
      child: Container(
        height: MediaQuery.of(context).size.height * 0.43,
        child: Column(
          children: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: TextFormField(
                decoration: InputDecoration(labelText: 'Número do Cartão'),
              ),
            ),
            Row(
              children: <Widget>[
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.08,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.25,
                  child: TextFormField(
                    decoration: InputDecoration(labelText: 'Validade'),
                  ),
                ),
                SizedBox(
                  width: 50,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.42,
                  child: TextFormField(
                    decoration:
                        InputDecoration(labelText: 'Código de Segurança'),
                  ),
                ),
              ],
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: TextFormField(
                decoration: InputDecoration(labelText: 'Nome Completo'),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: TextFormField(
                decoration: InputDecoration(labelText: 'CPF'),
              ),
            ),
            Row(
              children: <Widget>[
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.08,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.25,
                  child: TextFormField(
                    decoration: InputDecoration(labelText: 'Telefone'),
                  ),
                ),
                SizedBox(
                  width: 50,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.42,
                  child: TextFormField(
                    decoration:
                        InputDecoration(labelText: 'Data de Nascimento'),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class PageHeader extends StatelessWidget {
  final bool showNewAddress;
  final VoidCallback onTap;

  PageHeader({this.showNewAddress, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        GestureDetector(
          onTap: onTap,
          child: Container(
            child: Align(
              alignment: Alignment.topCenter,
              child: Column(
                children: <Widget>[
                  Text(
                    'Novo Endereço',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.blue),
                  ),
                  Icon(
                    !showNewAddress ? Icons.expand_more : Icons.expand_less,
                    color: Colors.blue,
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class MyAddressCards extends StatefulWidget {
  final double top;
  final bool showNewAddress;

  MyAddressCards({this.top, this.showNewAddress});

  @override
  _MyAddressCardsState createState() => _MyAddressCardsState();
}

class _MyAddressCardsState extends State<MyAddressCards> {
  int _isRadioSelected;
  final double top;

  _MyAddressCardsState({this.top});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isRadioSelected = 1;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 8, right: 8, bottom: 0, top: 0),
      child: Container(
        height: MediaQuery.of(context).size.height * 0.5,
        padding: EdgeInsets.all(8.0),
        child: ListView(
          children: <Widget>[
            CardEndereco(
              cardTitle: 'Casa',
              value: 1,
              groupValue: _isRadioSelected,
              telefone: '(35) 98422-8182',
              rua:
                  'Avenida Teste 1, Nº 458, Bloco 4 Ap 102',
              bairroCidade: 'Testando - Itajubá',
              onChanged: (newValue) {
                setState(() {
                  _isRadioSelected = newValue;
                });
              },
            ),
            Divider(),
            CardEndereco(
              cardTitle: 'Trabalho',
              value: 2,
              groupValue: _isRadioSelected,
              telefone: '(35) 9-999998888',
              rua: 'Avenida Teste 2 nº 55',
              bairroCidade: 'Testando - Itajubá',
              onChanged: (newValue) {
                setState(() {
                  _isRadioSelected = newValue;
                });
              },
            ),
            Divider(),
            CardEndereco(
              cardTitle: 'Trabalho 2',
              value: 3,
              groupValue: _isRadioSelected,
              telefone: '(35) 9-999998888',
              rua: 'Avenida Teste 3 nº 70',
              bairroCidade: 'Testando - Itajubá',
              onChanged: (newValue) {
                setState(() {
                  _isRadioSelected = newValue;
                });
              },
            ),
            Divider(),
            CardEndereco(
              cardTitle: 'Trabalho 3',
              value: 4,
              groupValue: _isRadioSelected,
              telefone: '(35) 9-999998888',
              rua: 'Avenida Teste 4 nº 90',
              bairroCidade: 'Testando - Itajubá',
              onChanged: (newValue) {
                setState(() {
                  _isRadioSelected = newValue;
                });
              },
            ),
            Divider(),
            CardEndereco(
              cardTitle: 'Trabalho 4',
              value: 5,
              groupValue: _isRadioSelected,
              telefone: '(35) 9-999998888',
              rua: 'Avenida Teste 5 nº 40',
              bairroCidade: 'Testando - Itajubá',
              onChanged: (newValue) {
                setState(() {
                  _isRadioSelected = newValue;
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}

class CardEndereco extends StatelessWidget {
  final int groupValue;
  final int value;
  final Function onChanged;
  final String rua;
  final String bairroCidade;
  final String telefone;
  final String cardTitle;

  CardEndereco(
      {this.groupValue,
      this.value,
      this.onChanged,
      this.rua,
      this.bairroCidade,
      this.telefone,
      this.cardTitle});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        Icons.contact_mail,
        size: 40,
      ),
      title: Text('${cardTitle}'),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('${rua}', style: TextStyle(fontSize: 12)),
          Text('${bairroCidade}', style: TextStyle(fontSize: 12)),
          Text('${telefone}', style: TextStyle(fontSize: 12))
        ],
      ),
      trailing: Radio(
        groupValue: groupValue,
        value: value,
        onChanged: (newValue) {
          onChanged(newValue);
        },
      ),
    );
  }
}

class PageContent extends StatelessWidget {
  final double top;

  PageContent({this.top});

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Padding(
      padding: EdgeInsets.all(8),
      child: Container(
        height: screenHeight * 0.51,
        child: Column(
          children: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: TextField(
                decoration: InputDecoration(labelText: 'Estado'),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: TextField(
                decoration: InputDecoration(labelText: 'Cidade'),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: TextField(
                decoration: InputDecoration(labelText: 'Bairro'),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.47,
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Logradouro'),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.25,
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Numero'),
                  ),
                ),
              ],
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: TextField(
                decoration: InputDecoration(labelText: 'Complemento'),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.34,
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Telefone'),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.4,
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Responsável'),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ShopCartList extends StatefulWidget {
  ShopCart shopCart;
  double total;

  ShopCartList({this.shopCart, this.total});

  @override
  _ShopCartListState createState() => _ShopCartListState();
}

class _ShopCartListState extends State<ShopCartList> {
  @override
  Widget build(BuildContext context) {
    ShopCart cart = widget.shopCart;
    double valorTotal = widget.total;

    if (cart.Items.length == 0) {
      return ListView(
        children: <Widget>[
          ListTile(
            title: Text('Nenhum produto no carrinho.'),
          )
        ],
      );
    }

    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
          ),
          height: MediaQuery.of(context).size.height * 0.5,
          child: ListView.builder(
            itemCount: cart.Items.length,
            itemBuilder: (BuildContext context, int index) {
              ShopItem item = cart.Items[index];
              return Slidable(
                actionPane: SlidableDrawerActionPane(),
                actionExtentRatio: 0.2,
                closeOnScroll: true,
                child: ListTile(
                  title: Text(item.produto.Nome),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            'Marca: ${item.produto.Marca}',
                            style: TextStyle(
                                color: Colors.red[900],
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            'Quantidade ${item.Quantidade}',
                            style: TextStyle(
                                color: Colors.black45,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            r'Valor Un. R$',
                            style: TextStyle(
                                color: Colors.black54,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            '${item.produto.Valor}',
                            style: TextStyle(
                                color: Colors.black54,
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            r'Valor Total: R$',
                            style: TextStyle(
                                color: Colors.black45,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            '${item.ValorTotal}',
                            style: TextStyle(
                                color: Colors.black45,
                                fontWeight: FontWeight.w500),
                          )
                        ],
                      ),
                    ],
                  ),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                secondaryActions: <Widget>[
                  IconSlideAction(
                    icon: Icons.add,
                    color: Colors.green[300],
                    caption: "1 Unidade",
                    onTap: () {
                      debugPrint('Quantidade ${item.Quantidade}');
                    },
                  ),
                  IconSlideAction(
                    icon: Icons.remove,
                    color: Colors.red[100],
                    caption: "1 Unidade",
                    onTap: () {},
                  ),
                  IconSlideAction(
                    icon: Icons.delete_forever,
                    color: Colors.red[500],
                    caption: "Remover",
                    onTap: () {
                      debugPrint('Add');
                    },
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
